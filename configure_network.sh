#!/bin/bash

#if [[ $EUID -ne 0 ]]; then
#	echo "requires root access to run ansible playbook"
#	exit 1
#fi

# create ansible inventory list
xdc ansible inventory > vars/hosts.ini

echo "[all:vars]
ansible_connection=ssh
ansible_user=test
ansible_ssh_pass=test
ansible_python_interpreter=/usr/bin/python3" >> vars/hosts.ini

# get all the link info
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i vars/hosts.ini ansible/get-link-info.yml

# 
./initialize-ansible.py


#
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i vars/hosts.ini --extra-vars "@vars/hosts.yml" ansible/addresses.yml
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i vars/hosts.ini --extra-vars "@vars/hosts.yml" ansible/routes.yml
