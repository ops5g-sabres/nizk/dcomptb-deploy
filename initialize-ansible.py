#!/usr/bin/python3

import os
import json
import yaml
from ipaddress import ip_network
from pprint import pprint

## Requires ./get-links.sh to be run first

neighbors = {}
special = {}

for fi in os.listdir('data'):
    if "xp-out" in fi:
        with open('data/'+fi) as a:
            x = json.load(a)
            for y in x:
                # b eth1.255 00:08:a2:0d:ca:09
                line = y["stdout"].split(" ")
                print(line)
                host = line[0]
                eth = line[1]
                addr = line[2]
                vlan = None
                if '.' in eth:
                    vlan = eth.split(".")[1]
                    eth = eth.split(".")[0]
                if vlan is not None:
                    if vlan not in neighbors:
                        neighbors[vlan] = []
                    neighbors[vlan] += [(host, eth, addr)]
                else:
                    special[host] = addr
pprint(neighbors)
# we will cheat a bit here, because on dcomp the child/vlan interfaces inherit the parent mac
#pprint(special)

counter24 = 0
connections = []
known= {"a":"x", "b": "y"} # these (x,y) dont have vlans in dcomptb because they are access ports
# create the ip addresses for our network
for lan in neighbors:

    local = list(ip_network('10.0.%d.0/24' % counter24))
    globl = list(ip_network('192.168.%d.0/24' % counter24))

    members = neighbors[lan]
    if len(members) < 2:
        member = neighbors[lan][0]
        node = member[0]
        eth = member[1] + '.' + lan
        link = member[2]
        if node in known:
            connections += [[(node, eth, str(globl[1]), link),(known[node], "eth1", str(globl[2]), link)]]
    else:
        mem0 = members[0]
        mem1 = members[1]
        eth0 = mem0[1] + '.' + lan
        eth1 = mem1[1] + '.' + lan
        node0 = mem0[0]
        node1 = mem1[0]
        link0 = mem0[2]
        link1 = mem1[2]
        connections += [[(node0, eth0, str(local[1]), link0),(node1, eth1, str(local[2]), link1)]]
    counter24 += 1

#print(connections)

# create the new ansible/vars file based on the dcomptb topo/interface names
cfg = {"config": {}}

for v in range(0, len(connections)):
    node0 = connections[v][0][0]
    node1 = connections[v][1][0]
    iface0 = connections[v][0][1]
    iface1 = connections[v][1][1]
    ip0 = connections[v][0][2]
    ip1 = connections[v][1][2]
    if node0 in cfg["config"]:
        cfg["config"][node0]["interfaces"] += [{"name": iface0, "ip": ip0}]
    else:
        cfg["config"][node0] = {"interfaces": [{"name": iface0, "ip": ip0}]}
    if node1 in cfg["config"]:
        cfg["config"][node1]["interfaces"] += [{"name": iface1, "ip": ip1}]
    else:
        cfg["config"][node1] = {"interfaces": [{"name": iface1, "ip": ip1}]}

# dump this config to ansible/vars to be used by ansible scripts
with open('vars/hosts.yml', 'w') as outfile:
        yaml.dump(cfg, outfile, default_flow_style=False)


hostMap = {}
# now create a file to help parse the mac address affiliations
for v in range(0, len(connections)):
    node0 = connections[v][0][0]
    node1 = connections[v][1][0]
    iface0 = connections[v][0][1]
    iface1 = connections[v][1][1]
    # heres our cheat noted above about parent interfaces
    link0 = special[node0]
    link1 = special[node1]
    if node0 not in hostMap:
        hostMap[node0] = {}
    hostMap[node0][iface0] = {link0: link1}
    if node1 not in hostMap:
        hostMap[node1] = {}
    hostMap[node1][iface1] = {link1: link0}

# now we dump this file for another program to manage/handle
#pprint(hostMap)
#with open('vars/click-neighbors.json', 'w') as outfile:
#        json.dump(hostMap, outfile)

